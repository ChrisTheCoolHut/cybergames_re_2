import angr, IPython

combo_names = ["combo_one", "combo_two", "combo_three"]

chal = "problems/street_fighter_7"

p = angr.Project(chal, auto_load_libs=False)

Cipher_func = p.loader.main_object.get_symbol("EVP_CipherInit_ex")

"""
Run from the start of the function to right before the
crypto decrypting stuff.
"""
for check_func in combo_names:
    symbol = p.loader.main_object.get_symbol(check_func)
    symbol_addr = symbol.rebased_addr

    state = p.factory.call_state(addr=symbol_addr)
    simgr = p.factory.simgr(state)
    simgr.explore(find=Cipher_func.rebased_addr)
    print("{} : inputs".format(check_func))
    for path in simgr.deadended:
        print(path.posix.dumps(0).decode("utf-8", "ignore"))
