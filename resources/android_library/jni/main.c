#define _GNU_SOURCE
#include <android/log.h>
#include <stdio.h>
#include <dlfcn.h>

int (*real_strcmp)(const char *s1, const char *s2) = NULL;

#define LOG_TAG "MY_LIB_HOOK"
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

int strcmp(const char *s1, const char *s2)
{
  LOGE("Inside of strcmp");
  LOGE("arg1 : %s", s1);
  LOGE("arg2 : %s", s2);

  if (real_strcmp == NULL)
  {
    real_strcmp = dlsym(RTLD_NEXT, "strcmp");
  }

  return real_strcmp(s1,s2);
}