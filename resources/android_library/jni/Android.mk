LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE	:= hello_world
LOCAL_SRC_FILES	:= main.c
LOCAL_LDLIBS	+= -llog # allow android logging
# Build an executable with the below flag
# include $(BUILD_EXECUTABLE)
# Build a shared object with the below flag
include $(BUILD_SHARED_LIBRARY)