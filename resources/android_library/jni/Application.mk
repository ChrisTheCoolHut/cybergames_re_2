# Compile for both arm and arm64 then x86 and x86_64
# If you're using an emulator you can change this to x86
APP_ABI			:= armeabi-v7a arm64-v8a x86 x86_64
APP_PLATFORM	:= android-21
APP_STRIP_MODE := none