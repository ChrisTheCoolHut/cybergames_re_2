
This is a native android project that requires one of the standalone NDK toolchains:
https://developer.android.com/ndk/downloads
Once you have it downloaded, add the ndk-build command to your path

## Build

```bash
$ ndk-build
ndk-build    
[armeabi-v7a] Compile thumb  : hello_world <= main.c
[armeabi-v7a] Executable     : hello_world
[armeabi-v7a] Install        : hello_world => libs/armeabi-v7a/hello_world
[arm64-v8a] Compile        : hello_world <= main.c
[arm64-v8a] Executable     : hello_world
[arm64-v8a] Install        : hello_world => libs/arm64-v8a/hello_world
[x86] Compile        : hello_world <= main.c
[x86] Executable     : hello_world
[x86] Install        : hello_world => libs/x86/hello_world
[x86_64] Compile        : hello_world <= main.c
[x86_64] Executable     : hello_world
[x86_64] Install        : hello_world => libs/x86_64/hello_world

```

```bash
$ cd android_library
$ adb push libs/arm64-v8a/libhello_world.so /data/local/tmp/
$ adb shell
$ su
# setenforce 0
# setprop wrap.com.example.hellojni LD_PRELOAD=/data/local/tmp/libhello_world.so
## Exit the shell
## Start the app
$ adb shell am start -n "com.example.hellojni/com.example.hellojni.HelloJni" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
### Enter a value and look for the check
$ adb logcat | grep MY_LIB_HOOK
```