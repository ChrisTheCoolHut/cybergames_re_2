# RE lecture 2

## Constraint Solving

This far into your reverse engineering journey I think you've heard about angr, symbolic execution, and constraints. This material will attempt to delve a little farther into how to use some of these tools and techniques on more complicated challenges.

### Slice Execution

Slice execution is where we take only part of a given problem that we're interested in and analyze only that part.

A "slice" in this excercise is a section of program code starting at one address within a single binary and ending at another address in the same binary.

Symbolic analysis is the process by which we lift, interpret, and translate an existing program in a set of equations.

Symbolic analysis frequently runs into an issue called the "state space explosion" issue. Where when analyzing a given binary, too many potential aveneues are execution are attempted to be analyzed which exhaust's the host system's memory and compute.

### Symbolic Slice Execution

To help guide symbolic analysis to only analyze the parts of a binary we are interested in, we can have it start further into a given binary and terminate analysis before something else causing state space explosion could occur.

### Example 1 -- Street Fighter 7

Several reverse engineering problems follow the format of "crackmes", where an input is expected and when a successful input is provided the flag is printed. This example is from the minictf last year and features a number of constraints that can be manually passed, or can automaticaally be passed with the right kind of symbolic execution.

The example is broken out into four "combos" or checks, and when the input condition is met the functions will decrypt and print a portion of the flag. Crypto code is famous for creating many analysis states that explode symbolic execution.

While combo one is trivial to solve without constraint solving, we'll still run through it to demonstrate the example. When opened in a decompiler and cleaned up that function will look similar to the code below:

```c
int combo_one()
{
    unsigned char in_text[] = {/* Crypt blob here */};
    printf("Oh no, a jump in!\nWhat do you do: ");
    fgets(user_input, 0x10, stdin);
    user_input[strcspn(user_input, "\n")] = '\x00';

    strncpy(key, user_input, sizeof(key));

    if (strncmp("623stLP", key, strlen("623stLP")) == 0)
    {
        // Do decryption here
        return 0;
    }
    return -1;
}
```

The key takeaways from the code above is that some user input is being retrieved from stdin through the `fgets` call, some sort of comparision is being made with the `strncmp` call, and then if the call is successful the program will attempt to decrypt a large binary blob (the first part of the flag output).

In this situation we would like the analyze only a slice of the program running from the `fgets` call only to the very start of the decryption.

## Constraint solving without limits

Okay so we've got one cool trick for when a binary gets too complicated, what about when we can't slice execute it? There is an old problem from the 2015 CTF "9447" called nobranch that is one gigantic basic block that just builds constraint after constraint. If you open it up in ghidra/binja/ida you'll see warning about how large the main function is.

When you know a tool like `angr` can solve a problem, but will take forever you need to set the solver timeout to something gigantic:

```python
state.solver._solver.timeout = 0xfffffff
```

It's buried under a hidden access, but adjusting this feature lets you run angr scripts that aren't state space exploding for much longer.

I've included the full solve in the examples and solves folder, but just wanted to note this trick here.

## Underflows with constraints

Sometimes we run across RE or PWN problems that require integer underflows. Below is a small `z3` snippet showing how to successfully model 64 bit integer representation. This is a snippet taken from my spaceheroes 2023 `jedi` solve where I needed to to ensure a value less than 5 led to an address I could control.

```python
from z3 import *
# 18446744073709551534 -> -82
x = BitVec('my_val', 64) 
y = BitVec('end_addr', 64)
s = Solver()
s.add(x < 5)
s.add(x > -99999999)
# 0x0000000000404410 (0x00000000004012d0)
s.add(y == 0x0000000000403768)
# [$RAX + ($RDI*8)]
s.add(0x00000000004046a0 + (x*8) == y) 
print(s.check())
print(s.model())
```

## Android problem solving

When I've come across android reversing problems they tend to fall into one of two categories:

 * Straight Java/Kotlin android reverseing
 * Android APK with some sort of native component

In theory there could be a third where they use the android NDK to compile to aarch64/arm for only android devices, but I haven't anyone do that yet.

### Regular android APK reverseing

Static APK reverseing tends to follow the normal RE path of figuring out how to unpack the file, figuring out how to disassemble it, and maybe figuring out how to run it.

I won't go into much detail here, but wanted to link a couple tools I can talk about more in-depth:

Static analysis:
 * Jadx : https://github.com/skylot/jadx
 * apktool : https://apktool.org/
Patching APKs:
 * DexPatcher : https://github.com/DexPatcher/dexpatcher-tool
Running APKs:
 * https://developer.android.com/studio/run/managing-avds
   * Note: Make sure you have an x86 and arm virtual device if you're trying to load these before a CTF

Several android problems can be solved with those tools alone, where if I can't quickly solve it statically, I'll try to patch the apk using DexPatcher to do something like log what the application is doing to give me more insight into the application.

### Android APK with native components

This class of reverse engineering problem tends to be formatted in a very specific way. The Java/Kotlin components of the APK will usually request an input, sequence of inputs, or some other form of user interaction that is then sent into a native library.

When Android sends data between an app and native code it uses the JNI (Java Native Interface). There is a lot of material out there discussing these interactions, but at a high level a JNI_env special object gets created and passed into a native c/c++ library that can then send information back in a similar way.

From the developer point of view we can see part of this interaction by specifying that a given function inside of a class is "native" (See https://github.com/fmtlib/android-ndk-example/blob/master/example/src/com/example/hellojni/HelloJni.java for an example.). On the other side, we can get data from the application and return it in a similar manner using special JNI functions. (See https://github.com/fmtlib/android-ndk-example/blob/master/example/jni/hello-jni.cpp)

### How to solve these problems

Provided alongside this lecture is a "app-native" problem that launches an android activity and prompts for a password.

This password is checked in a native library and based on the check will indicate whether or not the input was correct.

### Wrap

I'm pretty sure the only people that use this feature in Android write exploits for android devices. Android devices have "properties" which store system wide name-value pairs of information that any app can use.

The "wrap" property is a special property for apps that is designed to run a specific program before launching an app. There are really only two examples online using "logwrapper" and "asanwrapper". By using setprop like below you can set one of these programs to run before the application:

```bash
 # setprop wrap.com.example.hellojni logwrapper
```

However this actually just inserts "logwrapper" into the invoke application call in Zygote/System_sever which means this is really a command injection. So instead of specifying a binary, we can actually set environment variables!
```bash
 # setprop wrap.com.example.hellojni LD_PRELOAD=/path/to/library.so
```

This means for any native component of an android application that uses a symbol from another library, we can hook it! I've provided an example project that can hook strcmp and log the arguments used.

If we compile and push a library to our phone, we can tell the android APK that we're reverseing to load our library first on boot, and any calls the native code uses will get hooked!

```bash
$ cd android_library
$ adb push libs/arm64-v8a/libhello_world.so /data/local/tmp/
$ adb shell
$ su
# setenforce 0
# setprop wrap.com.example.hellojni LD_PRELOAD=/data/local/tmp/libhello_world.so
## Exit the shell
$ adb shell am start -n "com.example.hellojni/com.example.hellojni.HelloJni" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
$ adb logcat | grep MY_LIB_HOOK
```

#### Caveats

You might need to disable selinux:

```bash
 # setenforce 0
```

You also might need to push the library into the application's folder.
```bash
$ adb shell pm path com.example.hellojni
package:/data/app/~~v8bbyghH1cXCWENWLbiS0Q==/com.example.hellojni-dqiM_FYqwQOW_fJfQ4_7hQ==/base.apk
$ adb push mycool_lib.so /data/app/~~v8bbyghH1cXCWENWLbiS0Q==/com.example.hellojni-dqiM_FYqwQOW_fJfQ4_7hQ==/
$ adb shell su setprop wrap.com.example.hellojni /data/app/~~v8bbyghH1cXCWENWLbiS0Q==/com.example.hellojni-dqiM_FYqwQOW_fJfQ4_7hQ==/mycool_lib.so
```
### Frida

Frida is a dynamic binary instrumentation framework, that was originally designed for phones. It's got great support for iOS and Android and generally lets you poke at applications pretty well.

Get the latest frida server (https://github.com/frida/frida/releases)

https://github.com/chame1eon/jnitrace
```bash
jnitrace hellojni -l libhello-jni.so -m attach
```
The snippet below can tell you when a method is hit.
```bash
frida-trace -U -i "Java_*" hellojni
```